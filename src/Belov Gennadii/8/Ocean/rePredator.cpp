﻿#include "HeadPredator.h"
Сoordinates Predator::way() {
	random_shuffle(course.begin(), course.end()); // перемешиваем
												  // каждый раз движение происходит в случайном направлении
	Сoordinates temp = route("Prey");
	if (temp == Coor) {
		--timeEat;              // не нашёл пищу
		return Prey::way();         // ищет свободную клетку
	}		
	else {
		ocean->prey--;   // охота успешна
		return temp;
	}
}
void Predator::motion(Сoordinates tCoor) {
	if (tCoor == Coor)
		return;

	getOcean(tCoor) = { "Predator", getOcean(Coor).second };	
	getOcean(Coor) = { "Water", nullptr };

	if (!timeBirth) {
		getOcean(Coor) = { "Predator", make_shared<Predator>(Coor, ocean) };		
		ocean->predator++;
		timeBirth = cycle;
	}
	Coor = tCoor;
}