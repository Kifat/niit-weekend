﻿#ifndef _HeadConst_H_
#define _HeadConst_H_
// начальное кол-во объектов
const int startStone = 75;
const int startPrey = 150;
const int startPredator = 20;

const int cycle = 7;   // рождение
const int cycleL = 6;  // кормёжка
// параметры океана
const int maxWidth = 75;
const int maxHeight = 25;
#endif // !_HeadConst_H_