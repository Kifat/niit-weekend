﻿#ifndef _HeadPredator_H_
#define _HeadPredator_H_
#include "HeadPrey.h"
// класс объектов наследующих способности класса Prey
      //  при этом объекты приобретают возможность охотится на объекты наследуемого класса
class Predator : public Prey {
private:
	int timeEat = cycleL;
public:
	Predator(Сoordinates tCoor, Ocean * oc) : Prey(tCoor, oc) {}
	virtual ~Predator() {};
	virtual void life() {		
		if (!timeEat) {			   // исчезает, не найдя пищу
			ocean->predator--;                      // сначала уменьшается счётчик, потом уничтожается объект
			getOcean(Coor) = { "Water", nullptr };			
			return;
		}		
		if (timeBirth)
			--timeBirth;
		motion(way());
	}
	virtual Сoordinates way();
	virtual void motion(Сoordinates tCoor);
};
#endif // !_HeadPredator_H_