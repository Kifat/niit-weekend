﻿#ifndef _HeadOcean_H_
#define _HeadOcean_H_
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <ctime>
#include <map>
#include <utility>
#include <algorithm>
#include <windows.h>
#include "HeadConst.h"
using namespace std;

// для координат
struct Сoordinates {
	int x;
	int y;
	bool operator == (const  Сoordinates& n) const { return (n.x == x && n.y == y) ? 1 : 0; }
};

class Cell;
class Prey;
class Stone;
class Predator;

typedef pair< string, shared_ptr<Cell> > Space;
enum Way { North, East, South, West };

class Ocean
{
	friend class Cell;	
private:		
	Space cells[maxHeight][maxWidth];	  // океан
	vector<Сoordinates> v;               // вектор возможных координат для рандомного заполнения
public:		
	void fill(int startStone, int startPrey, int startPredator);     // заполнение океана
	void develop();                // его дальнейшее развитие
	void art();                    // отображение на экране
	// кол-во объектов и шаг цикла
	int prey = startPrey;           
	int predator = startPredator;
	int step = 0;
};
#endif // !_HeadOcean_H_