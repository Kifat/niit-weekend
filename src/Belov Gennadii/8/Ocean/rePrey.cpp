﻿#include "HeadPrey.h"

Сoordinates Prey::way() {
	random_shuffle(course.begin(), course.end()); // перемешиваем
												  // каждый раз движение происходит в случайном направлении	
	return route("Water");     // ищет участок с водой
}
void Prey::motion(Сoordinates tCoor) {
	if (tCoor == Coor)   // остаётся на месте
		return;

	getOcean(tCoor) = { "Prey", getOcean(Coor).second };     // объект перемещается на новую клетку	   
	getOcean(Coor) = { "Water", nullptr };                   // затираются данные на прежнем месте

	if (!timeBirth) {
		getOcean(Coor) = { "Prey", make_shared<Prey>(Coor, ocean) };   // рождение нового объекта	
		ocean->prey++;
		timeBirth = cycle;
	}

	Coor = tCoor;         // при перемещении изменяются координаты объекта
}

Сoordinates Prey::route(string field) {
	for (auto el : course) {
		Сoordinates temp;
		switch (el)               // выбор направления
		{
		case North:
			if (Coor.x != 0)               //     круговорот
				temp = { Coor.x - 1, Coor.y };
			else temp = { maxHeight - 1, Coor.y };
			//temp = Coor.x != 0 ? { Coor.x - 1, Coor.y } : { Coor.x - 1, Coor.y };
			break;
		case East:
			if (Coor.y != maxWidth - 1)
				temp = { Coor.x, Coor.y + 1 };
			else temp = { Coor.x, 0 };
			break;
		case South:
			if (Coor.x != maxHeight - 1)
				temp = { Coor.x + 1, Coor.y };
			else temp = { 0, Coor.y };
			break;
		case West:
			if (Coor.y != 0)
				temp = { Coor.x, Coor.y - 1 };
			else temp = { Coor.x, maxWidth - 1 };
			break;
		default:
			break;
		}
		if (getOcean(temp).first == field) {
			return temp;
		}
	}
	return Coor;           // некуда ходить
}