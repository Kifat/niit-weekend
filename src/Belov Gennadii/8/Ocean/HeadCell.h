﻿#ifndef _HeadCell_H_
#define _HeadCell_H_
#include "HeadOcean.h"
// базовый абстрактный класс
class Cell
{
	friend class Ocean;
protected:
	Сoordinates Coor; //координаты объекта
	Ocean * ocean;	 // указатель на океан
public:
	virtual ~Cell() {};	
	virtual void life() = 0;
	// для нахождения смежных клеток
	Space& getOcean(Сoordinates _Сoor) { return ocean->cells[_Сoor.x][_Сoor.y]; }
	Ocean * getOcean() { return ocean; }
};
#endif // !_HeadCell_H_