﻿#ifndef _HeadStone_H_
#define _HeadStone_H_
#include "HeadCell.h"
class Stone : public Cell {
public:
	Stone(Сoordinates tCoor, Ocean * oc) { Coor = tCoor; ocean = oc; }
	virtual ~Stone() {};
	virtual void life() {};
};

#endif // !_HeadStone_H_
