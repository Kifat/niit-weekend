﻿#include "HeadOcean.h"
#include "HeadCell.h"
#include "HeadPrey.h"
#include "HeadPredator.h"
#include "HeadStone.h"

map <string, char> Art = { { "Water", '~' },{ "Stone", '#' },{ "Prey", '*' },{ "Predator", '@' } };
map <string, int> Color = { { "Water", 11 },{ "Stone", 10 },{ "Prey", 14 },{ "Predator", 12 } };
void Ocean::art() {
	/*system("cls");
	for (auto &el1 : cells) {
		for (auto &el2 : el1) {
			cout << Art.at(el2.first);
		}
		cout << endl;
	}
	Sleep(500);*/
	COORD bufferSize = { 75, 26 };
	SMALL_RECT writeArea = { 0,0,75,26 };
	COORD charPosition = { 0,0 };
	CHAR_INFO consBuffer[26 * 75] = {};
	int i = 0;
	string str = " / Prey(*): " + to_string(prey) + " / Predator(@): " + to_string(predator) + " / Cycle: " +
		to_string(step);
	// океан
	for (auto &el1 : cells) {
		for (auto &el2 : el1) {
			//cout << Art.at(el2.first);
			consBuffer[i].Char.AsciiChar = Art.at(el2.first);			
			consBuffer[i++].Attributes = Color.at(el2.first);
		}		
	}
	// значения
	for (auto el : str) {
		consBuffer[i].Char.AsciiChar = el;
		consBuffer[i++].Attributes = 7;
	}
	WriteConsoleOutput(GetStdHandle(STD_OUTPUT_HANDLE), consBuffer, bufferSize, charPosition, &writeArea);
	Sleep(1000);
}

void Ocean::fill(int startStone, int startPrey, int startPredator) {
	for (int i = 0; i < maxHeight; i++) {        // создание координат
		for (int j = 0; j < maxWidth; j++) {
			Сoordinates temp;
			temp.x = i;
			temp.y = j;
			v.push_back(temp);
		}
	}
	random_shuffle(v.begin(), v.end()); // перемешиваем последовательность координат									
	// заполнение океана происходит по случайным координатам 
	for (auto el : v) {
		if (startStone-- > 0) {			
			cells[el.x][el.y] = { "Stone", make_shared<Stone>(el, this) };				
		}
		else if (startPrey-- > 0) {			
			cells[el.x][el.y] = { "Prey", make_shared<Prey>(el, this) };				
		}
		else if (startPredator-- > 0) {			
			cells[el.x][el.y] = { "Predator", make_shared<Predator>(el, this) };				
		}
		else cells[el.x][el.y] = { "Water", nullptr };       // Вода, вода, кругом вода
		                                             // вода не содержит в себе объекты
	}
	art();   // первое отображение
}

void  Ocean::develop() {
	while (1) {
		random_shuffle(v.begin(), v.end()); // перемешиваеаем
		                                    // движение объектов происходит в произвольном порядке
		for (auto el : v) {
			if (cells[el.x][el.y].first != "Water" && cells[el.x][el.y].first != "Stone")
				cells[el.x][el.y].second->life();
		}
		step++;
		art();  // вывод на косоль
	}
}