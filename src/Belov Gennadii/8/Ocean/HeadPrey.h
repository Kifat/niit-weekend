﻿#ifndef _HeadPrey_H_
#define _HeadPrey_H_
#include "HeadCell.h"
// класс объектов способных к перемещению и размножению
class Prey : public Cell {
protected:
	vector<int> course = { North , East, South, West };
	int timeBirth = cycle;
public:
	Prey(Сoordinates tCoor, Ocean * oc) { Coor = tCoor; ocean = oc; }
	virtual ~Prey() {};
	virtual void life() {
		if (timeBirth)  // новый объект мог не появится из-за высокой плотности
			--timeBirth;
		motion(way());
	}
	virtual Сoordinates way();    // выбор пути
	virtual void motion(Сoordinates tCoor);    // перемещение / рождение
	Сoordinates route(string);  // проверка направления и границ
};
#endif // !_HeadPrey_H_