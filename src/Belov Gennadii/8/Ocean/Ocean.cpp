﻿// Программа демонстрирует работу системы имитационного моделирования Океан
#include "HeadOcean.h"
int main() {
	setlocale(LC_ALL, "Russian");
	srand(time(0));	
	Ocean c;	
	c.fill(startStone, startPrey, startPredator);  // заполнение
	c.develop();	  // моделирование развития
	return 0;
}