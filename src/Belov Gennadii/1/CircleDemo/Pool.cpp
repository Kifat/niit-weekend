﻿#include "Circle.h"

void costPool() {
	Circle Pool, Track;
	const double radPool = 3., radTrack = radPool + 1; // ширина бетонной дорожки вокруг бассейна 1 м
	Pool.setRadius(radPool);
	Track.setRadius(radTrack);
	double costTrack, costFence;
	cout << "Укажите стоимость 1 квадратного метра бетонного покрытия: ";
	checkInput(costTrack);
	
	cout << "Укажите стоимость 1 погонного метра ограды: ";
	checkInput(costFence);
	
	cout << "Стоимость материалов для бетонной дорожки вокруг бассейна равна: "
		<< fixed << setprecision(3)
		<< costTrack * (Track.getArea() - Pool.getArea()) + costFence * Pool.getFerence()
		<< endl;
}