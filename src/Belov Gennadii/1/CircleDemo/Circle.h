﻿#ifndef _Circle_H_
#define _Circle_H_
#include <iostream>
#include <iomanip>
#include <clocale>

using namespace std;

class Circle {
private:
	double Radius, Ference, Area;
	//static const double PI;
public:
	Circle(): Radius(0), Ference(0), Area(0) {}
	void setRadius(double);
	void setFerence(double);
	void setArea(double);
	double getRadius() const { return Radius; }
	double getFerence() const { return Ference; }
	double getArea() const { return Area; }
};
void Earth_Rope();
void costPool();
void checkInput(double&);
#endif // !_Circle_H_