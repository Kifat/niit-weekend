﻿#include "HeadDate.h"

const string DateTime::Months[12] = {
	"Января", "Февраля", "Марта", 
	"Апреля", "Мая", "Июня", "Июля",
	"Августа", "Сентября", "Октября", "Ноября", "Декабря" };

const string DateTime::WeekDays[7] = { 
	"Воскресенье", "Понедельник", "Вторник", 
	"Среда", "Четверг", "Пятница", "Суббота" };

void DateTime::printToday() const {
	/*char str[80];
	strftime(str, 80, "Сегодня: %A %d %B %Y года\n", Date);
	cout << str;*/
	cout << "Сегодня: ";
	printDate(Date);
}

void DateTime::printYesterday() const {
	DateTime d = *this;
	--d.Date->tm_mday;
	adjustmentDate(d.Date);
	cout << "Вчера: ";
	printDate(d.Date);
}

void DateTime::printTomorrow() const {
	DateTime d = *this;
	++d.Date->tm_mday;
	adjustmentDate(d.Date);
	cout << "Завтра: ";
	printDate(d.Date);
}

void DateTime::printFuture(const int day) const {
	DateTime d = *this;
	d.Date->tm_mday += day;
	adjustmentDate(d.Date);
	cout << "В будущем (" << day << "д) ";
	printDate(d.Date);
}

void DateTime::printPast(const int day) const {
	DateTime d = *this;
	d.Date->tm_mday -= day;
	adjustmentDate(d.Date);
	cout << "В прошлом (" << day << "д) ";
	printDate(d.Date);
}

void DateTime::printMonth() const {
	char str[80];
	strftime(str, 80, "Месяц: %B\n", Date);
	cout << str;
}

void DateTime::printWeekDay() const {
	cout << "День недели: " << WeekDays[Date->tm_wday] << endl;
}

void DateTime::printCalcDif(const DateTime& date1, const DateTime& date2) {
	cout << "Количество дней с " <<	date1.printCalc() << " по " << date2.printCalc() <<
		" равно: " << DateTime::calcDifference(date1, date2) << endl;
}

unsigned long int DateTime::calcDifference(const DateTime date1, const DateTime date2) {
	long int day = date1.adjustmentCalc() - date2.adjustmentCalc();
	return day > 0 ? day : ~day + 1;
}

string DateTime::printCalc() const {
	return to_string(this->Date->tm_mday) + ' ' + Months[this->Date->tm_mon] +
		                   ' ' + to_string(this->Date->tm_year + 1900) + " года";
}

void DateTime::printDate(tm* date) const {
	cout << WeekDays[date->tm_wday] << ' '
		<< date->tm_mday << ' ' << Months[date->tm_mon] << ' '
		<< date->tm_year + 1900 << " года" << endl;
}

void DateTime::adjustmentDate(tm* date) const {
	if (date->tm_year < 70 || date->tm_year > 1099) {
		int year = 0;
		int f = 1;
		while (date->tm_year < -400) {
			year += 400;
			date->tm_year += 400;
		}
		while (date->tm_year > 1099) {
			year -= 400;
			date->tm_year -= 400;
		}
		if (date->tm_year < 70) {
			
			int epoch1 = (date->tm_year + 1900) / 100 + 1;
			if (date->tm_year < 0) {
				if (!(epoch1 % 4)) {
					year += 5;
					date->tm_year += 5;
					f = 0;
				}
			}
			while (date->tm_year < 70) {
				int epoch2 = (date->tm_year + 1899) / 100 + 1;
				if (epoch2 != epoch1) {

					if ((epoch1 % 4)) {
						year += epoch2 - epoch1;
						date->tm_year += epoch2 - epoch1;
					}
					epoch1 = epoch2;
					if (f) {
						year += 11;
						date->tm_year += 11;
					}
				}
				year += 28;
				date->tm_year += 28;
			}

		}
		mktime(date);
		date->tm_year -= year;
	}
	else mktime(date);
}

long int DateTime::adjustmentCalc() const {
	if (this->Date->tm_year < 70 || this->Date->tm_year > 1099) {
		int year = 0;
		int epoch1 = (this->Date->tm_year + 1900) / 100 + 1;
		int dayEp = 0;
		if (this->Date->tm_year > 1099) 
			dayEp -= 2; 
		else --dayEp;
		while (this->Date->tm_year < 70) {
			int epoch2 = (this->Date->tm_year + 1900) / 100 + 1;
			if (epoch2 != epoch1) {

				if ((epoch1 % 4))
					++dayEp;

				epoch1 = epoch2;
			}
			year += 28;
			this->Date->tm_year += 28;
		}
		while (this->Date->tm_year > 1099) {
			
			int epoch2 = (this->Date->tm_year + 1900) / 100 + 1;
			if (epoch2 != epoch1) {

				if ((epoch1 % 4))
					--dayEp;

				epoch1 = epoch2;
			}
			year -= 28;
			this->Date->tm_year -= 28;
		}
		return mktime(this->Date) / (24 * 60 * 60) - (year / 28 * 21 * 365 + year / 28 * 7 * 366 - dayEp);
	}
	else return mktime(this->Date) / (24 * 60 * 60) - 1;
}