﻿#include "HeadDate.h"

int main() {
	setlocale(LC_ALL, "Russian");
	DateTime d(1, 3, 1), d2(7,1,7417);
	const int day = 5;
	const long long unsigned int y = 8249756972091;
	d.printToday();
	d.printYesterday();
	d.printTomorrow();
	d.printFuture(day);
	d.printPast(day);
	d.printMonth();
	d.printWeekDay();
	DateTime::printCalcDif(d, d2);
	return 0;
}