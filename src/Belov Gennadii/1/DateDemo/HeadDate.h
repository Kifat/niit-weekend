﻿#ifndef _Date_H_
#define _Date_H_
#include <ctime>
#include <iostream>
#include<string>
#include <clocale>
using namespace std;

class DateTime {
private:
	tm* Date;
	static const string Months[12];
	static const string WeekDays[7];

public:

	// конструкторы
	DateTime() { 
		tm* d;
		time_t t = time(0);
		d = localtime(&t);
		Date = new tm(*d);
		//cout << "Creation\n";
	}

	DateTime(int day, int month, int year): DateTime() {
		Date->tm_mday = day;
		Date->tm_mon = month - 1;
		Date->tm_year = year - 1900;
		adjustmentDate(Date);		
	}

	DateTime(const DateTime& obj)  {  
		Date = new tm(*obj.Date);	
		//cout << "Copying\n";
	}

	~DateTime() {
		delete Date;
		Date = nullptr;
		//cout << "Delete\n"; 
	}
	//
	// иные методы
	void printToday() const;
	void printYesterday() const;
	void printTomorrow() const;
	void printFuture(const int) const;
	void printPast(const int) const;
	void printMonth() const;
	void printWeekDay() const;
	static unsigned long int calcDifference(const DateTime, const DateTime);
	void printDate(tm* d) const;
	void adjustmentDate(tm*) const;
	long int adjustmentCalc() const;
	static void printCalcDif(const DateTime&, const DateTime&);
	string printCalc() const;
};

#endif // !_Circle_H_