﻿#include "Automata.h"

int main()
{
	setlocale(LC_ALL, "Russian");
	Automata au1;
	au1.off();	// загружаются накопленные данные
	au1.on();      // включение \ пароль: gfhjkm
	au1.coin(150);     // внос денег
	au1.choice(5);      // выбор напитка
	//au1.off();   // при выключении после работы записываются статистические данные
	return 0;
}