﻿// Проект DekanatDemo / версия без friend классов
#include "HeadDekanat.h"

int main() {
	setlocale(LC_ALL, "Russian");
	srand(time(0));
	vector<string> files = { "Groups.txt", "Students.txt" };
	Dekanat d1(files, 0);   // создание деканат / 0 это начальные спики студентов и групп
	d1.grouping();        // случайное распределение студентов
	d1.AddRandMark();      // добавление случайных оценок студентам
	d1.ElectStudentGr();    // инициация выборов старост в группах
	d1.StudentTransfer(d1.getStud(74), d1.getGroup(2)); // перевод студента из группы в группу - без friend придётся прописывать
	d1.exStudentsAP();   //отчисление студентов за неуспеваемость
	d1.AcademicPerformance(); // накопление статистики по успеваемости студентов и групп
	d1.inFile();  //сохранение обновленных данных в файлах
	d1.inConsole(); //вывод данных на консоль
	
	return 0;
}