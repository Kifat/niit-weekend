﻿#include "HeadDekanat.h"

/////////////////
// Student
void Student::EnlistmentGroup(Group *groupa)                      // зачисление в группу
{
	if (group == groupa)               // уже в этой группе
		return;
	if (group != nullptr && groupa != nullptr) {             // при переводе/исключении из группы
		group->exStudent(this);

	}
	group = groupa;
	if (groupa != nullptr) {
		for (auto el : group->getStudents()) {
			if (el == this)
				return;
		}
		group->AddStudent(this);

	}
}

void Student::AddMark(int mark) {                           // добавление оценки
	if (Num < 5)
		Marks[Num++] = mark;
}

int Student::sumMarks() const {                         // сумма всех оценок
	int sumMarks = 0;
	for (int i = 0; i < Num; i++)
		sumMarks += Marks[i];
	return sumMarks;
}

float Student::MidMark() const {                           // вычисление средней оценки
	if (!Num)
		return 0;
	else
		return (float)sumMarks() / Num;
}


//////////////
// Group
void Group::AddStudent(Student *student)             // добавление студента в группу
{	  
	if (prStudent(student))          // студент уже в группе
		return;
	student->EnlistmentGroup(this);
	Students.push_back(student);
	Num++;
}

void Group::exStudent(Student *student) {                                 // исключение студента из группы
	for (auto i = Students.begin(); i < Students.end(); ++i) {
		if (student == *i) {
			student->EnlistmentGroup(nullptr);
			Students.erase(i);
			--Num;
			if (student == HeadStudent)
				ElectStudent();
			return;
		}
	}
}

// поиск студента по ФИО или ИД
Student* Group::SearchStudent(int id) const {
	for (auto el : Students) {
		if (el->getID() == id)
			return el;
	}
	return nullptr;
}
Student* Group::SearchStudent(string fio) const {
	for (auto el : Students) {
		if (el->getFio() == fio)
			return el;
	}
	return nullptr;
}

float Group::MidMarkGroup() const {               // вычисление среднего балла в группе
	float sumMark = 0.;
	for (auto el : Students) {
		sumMark += el->MidMark();
	}
	return sumMark / Num;
}

bool Group::prStudent(Student *student) {
	int x = 0;
	for (auto el : Students)              
		if (el == student) {
			x = 1;
			break;
		}
	return x;
}

string Group::getStud() const {
	string str = "\n";
	for (auto i = 0; i != Students.size(); ++i) {
		str += to_string(Students[i]->getID()) + "/ " + Students[i]->getFio() +
			"/ ср. бал: " + to_string(Students[i]->MidMark()) + "\n";
	}
	return str;
}


///////////////
// Dekanat
Dekanat::Dekanat(vector<string> nameFile, int x) {                    // x = 0 - первоначальные списки
	if (!x) {
		ifstream fGroups(nameFile[0]), fStudents(nameFile[1]);
		if (fGroups.fail() || fStudents.fail())
			cerr << "file error" << endl;

		while (!fGroups.eof()) {
			string group;
			getline(fGroups, group);
			Groups.push_back(make_unique<Group>(group, this));
		}
		fGroups.close();

		while (!fStudents.eof()) {
			string student;
			getline(fStudents, student);
			Students.push_back(make_unique<Student>(student, this));
		}
		fStudents.close();
	}
};

void Dekanat::AddRandMark() {              // добавление случайных оценок студентам
	auto end = Students.end();
	for (auto i = Students.begin(); i != end; ++i) {
		for (int j = 0; j < 5; j++)
			(*i)->AddMark(rand() % 5 + 2);
	}
}

void Dekanat::AcademicPerformance() const {         // накопление статистики по успеваемости студентов и групп
	ofstream stat1("statistics.txt");
	if (!stat1)
		cerr << "file error" << endl;
	float midMarkStudents = 0.;
	int i = 0;
	while (i != Students.size()) {
		midMarkStudents += Students[i++]->MidMark();
	}
	midMarkStudents /= i;

	i = 0;
	float midMarkGroups = 0.;
	while (i != Groups.size()) {
		midMarkGroups += Groups[i++]->MidMarkGroup();
	}
	midMarkGroups /= i;

	stat1 << "Средняя успеваемость всех студентов: " << midMarkStudents <<
		"\nСредняя успеваемость групп: " << midMarkGroups << endl;
	stat1.close();
}

void Dekanat::StudentTransfer(Student *student, Group *newGroup) {   // перевод студентов из группы в группу
	if (prStudent(student) && prGroup(newGroup)) {
		student->EnlistmentGroup(newGroup);
	}
}
// проверка на присутствие студента и группы в деканате
bool Dekanat::prStudent(Student *student) {
	int x = 0;
	auto end = Students.end();
	for (auto i = Students.begin(); i != end; ++i) {
		if ((*i).get() == student) {
			x = 1;
			break;
		}
	}
	return x;
}
bool Dekanat::prGroup(Group *group) {
	int x = 0;
	auto end = Groups.end();
	for (auto i = Groups.begin(); i != end; ++i) {
		if ((*i).get() == group) {
			x = 1;
			break;
		}
	}
	return x;
}

void Dekanat::ElectStudentGr() {         // инициация выборов старост в группах
	int i = 0;
	while (i != Groups.size()) {
		Groups[i++]->ElectStudent();
	}
}

void Dekanat::exStudentsAP() {                   // отчисление студентов за неуспеваемость
	for (auto i = Students.begin(); i != Students.end(); ++i) {
		for (auto i = Students.begin(); i != Students.end(); ++i) {
			if ((*i)->MidMark() < 3) {
				(*i)->getGroup()->exStudent((*i).get());
				Students.erase(i);
				break;
			}
		}
	}	
}

void Dekanat::inConsole() const {             //вывод данных на консоль
	auto end = Groups.end();
	for (auto i = 0; i != Groups.size(); ++i) {
		cout << "\n\tгруппа: " << Groups[i]->getTitle() << Groups[i]->getStud() << endl;
	}
}

void Dekanat::inFile() const {             //сохранение обновленных данных в файлах
	auto end = Groups.end();
	for (auto i = 0; i != Groups.size(); ++i) {
		ofstream stat2(Groups[i]->getTitle() + ".txt");
		if (!stat2)
			cerr << "file error" << endl;
		stat2 << Groups[i]->getStud() << endl;
		stat2.close();
	}
}

void Dekanat::grouping() {               // случайное распределение студентов
	for (auto i = 0; i != Groups.size(); ++i) {
		for (int j = 0; j < 30; j++) {
			int n;
			do {
				n = rand() % Students.size();
			} while (Students[n]->getGroup() != nullptr);
			Students[n]->EnlistmentGroup(Groups[i].get());
		}
	}
}