﻿#include "HeadVeLoInt.h"

bool VeLoInt::operator > (const VeLoInt& n) const {
	auto n1 = number.begin();
	auto n2 = n.number.begin();

	int x = 1;
	if (number.length() == n.number.length()) {
		while (*n1 == *n2) {
			if (n1 == --number.end())
			{
				x = 0; break;
			}
			++n1; ++n2;
		}
		if (!x) return x;
		x = *n1 > *n2 ? 1 : 0;
	}
	else
		x = number.length() > n.number.length() ? 1 : 0;
	return x;
}

bool VeLoInt::operator >= (const VeLoInt& n) const {
	auto n1 = number.begin();
	auto n2 = n.number.begin();

	int x = 0;
	if (number.length() == n.number.length()) {
		while (*n1 == *n2) {
			if (n1 == --number.end())
			{
				x = 1; break;
			}
			++n1; ++n2;
		}
		if (x) return x;
		x = *n1 > *n2 ? 1 : 0;
	}
	else
		x = number.length() > n.number.length() ? 1 : 0;
	return x;

}

bool VeLoInt::operator < (const VeLoInt& n) const {
	auto n1 = number.begin();
	auto n2 = n.number.begin();

	int x = 1;
	if (number.length() == n.number.length()) {
		while (*n1 == *n2) {
			if (n1 == --number.end())
			{
				x = 0; break;
			}
			++n1; ++n2;
		}
		if (!x) return x;
		x = *n1 < *n2 ? 1 : 0;
	}
	else
		x = number.length() < n.number.length() ? 1 : 0;
	return x;
}

bool VeLoInt::operator <= (const VeLoInt& n) const {
	auto n1 = number.begin();
	auto n2 = n.number.begin();

	int x = 0;
	if (number.length() == n.number.length()) {
		while (*n1 == *n2) {
			if (n1 == --number.end())
			{
				x = 1; break;
			}
			++n1; ++n2;
		}
		if (x) return x;
		x = *n1 < *n2 ? 1 : 0;
	}
	else
		x = number.length() < n.number.length() ? 1 : 0;
	return x;

}

bool VeLoInt::operator == (const VeLoInt& n) const {
	auto n1 = number.begin();
	auto n2 = n.number.begin();

	int x = 0;
	if (number.length() == n.number.length()) {
		while (*n1 == *n2) {
			if (n1 == --number.end())
			{
				x = 1; break;
			}
			++n1; ++n2;
		}
	}
	return x;
}

bool VeLoInt::operator != (const VeLoInt& n) const {
	int x = 1;
	if (*this == n)
		x = 0;
	return x;
}

/*bool VeLoInt::operator ! () const {
int x = 0;
if (number == "0")
x = 1;
return x;
}

bool VeLoInt::operator && (const VeLoInt& n) const {
int x = 1;
if (number == "0" || n.number == "0")
x = 0;
return x;
}

bool VeLoInt::operator || (const VeLoInt& n) const {
int x = 1;
if (number == "0" && n.number == "0")
x = 0;
return x;
}*/

VeLoInt VeLoInt::operator * (int n) const {
	int fl = 0;
	if (n < 0) {
		n = 0 - n;
		fl = 1;
	}
		
	VeLoInt temp = "0";
	while (n--)
		temp = temp + *this;
	return !fl ? temp : -temp;
}

VeLoInt VeLoInt::operator * (const VeLoInt& n) const {
	if (number == "0" || n.number == "0")
		return "0";
	VeLoInt temp = "0";
	VeLoInt temp2 = "0";
	string tstr = "";
	for (auto i = n.number.rbegin(); i != n.number.rend(); i++) {
		//temp = ym(*i - '0');
		temp = *this * (*i - '0');
		temp.number += tstr;
		temp2 = temp2 + temp;
		tstr += "0";
	}
	if (temp2.sign == '-')
		temp2.sign = '+';
	if (sign != n.sign)
		return -temp2;
	else return temp2;
}

VeLoInt VeLoInt::operator / (const VeLoInt& n) const {
	if (!n) {
		cerr << "Error: DivByZero" << endl;
		exit(1);
	}
	auto length1 = number.length();
	auto length2 = n.number.length();
	if (length1 < length2)
		return "0";
	if (*this < n)
		return 0;
	int dnum = 18;
	string x = sign != n.sign ? "-" : "";
	
	if (length1 < dnum && length2 < dnum)
		return x + to_string(stoll(number) / stoll(n.number));
	else
		return x + div(n.number, 1);
}

VeLoInt VeLoInt::operator % (const VeLoInt& n) const {
	auto length1 = number.length();
	auto length2 = n.number.length();
	int dnum = 18;
	if (*this < n)
		return *this;
	if (length1 < dnum && length2 < dnum)
		return to_string(stoll(number) % stoll(n.number));
	else
		return	div(n.number, 0);
}

VeLoInt VeLoInt::operator ^ (int degree) {
	if (!degree)
		return 1;
	
	if (degree < 0) {
		return 0;
	}
	else {
		VeLoInt temp = *this;
		for (int i = 1; i < degree; i++) {
			temp *= *this;
		}
		return temp;
	}	
	
}

VeLoInt VeLoInt::operator ^ (const VeLoInt& degree) {
	if (!degree)
		return 1;
	if (degree < 0) {
		return 0;
	}
	VeLoInt temp = *this;
	VeLoInt i = 1;
	while (i < degree) {
		temp *= *this;
		++i;
	}
	return temp;
}

string VeLoInt::sum(string number2) const {
	string numberR = {};
	char num1, num2, numR;
	num1 = num2 = 0;
	auto n1 = --number.end();
	auto n2 = --number2.end();
	int flag, flag1, flag2;
	flag = flag1 = flag2 = 0;
	while (1) {
		if (flag1 && flag2) {
			if (flag)
				numberR = "1" + numberR;

			break;
		}

		if (n1 > number.begin())
			num1 = *n1-- - '0';
		else {
			num1 = !flag1 ? *n1 - '0' : 0;
			flag1 = 1;
		}
		if (n2 > number2.begin())
			num2 = *n2-- - '0';
		else {
			num2 = !flag2 ? *n2 - '0' : 0;
			flag2 = 1;
		}
		numR = num1 + num2;
		if (flag)
			numR++;
		if (numR > 9) {
			numR = numR % 10 + '0';
			flag = 1;
		}
		else {
			numR += '0';
			flag = 0;
		}
		numberR = numR + numberR;


	}
	return numberR;
}

string VeLoInt::diff(string number2) const {
	string numberR = {};
	char num1, num2, numR;
	num1 = num2 = 0;
	auto n1 = number.begin();
	auto n2 = number2.begin();

	int x;
	if (number.length() == number2.length()) {
		while (*n1 == *n2) {
			if (n1 == --number.end() && n2 == --number2.end())
				return numberR + '0';
			++n1; ++n2;
		}
		x = *n1 > *n2 ? 0 : 1;
	}
	else
		x = number.length() > number2.length() ? 0 : 1;

	n1 = --number.end();
	n2 = --number2.end();

	int flag, flag1, flag2;
	flag = flag1 = flag2 = 0;
	while (1) {
		if (flag1 && flag2)
			break;
		if (n1 > number.begin())
			num1 = *n1-- - '0';
		else {
			num1 = !flag1 ? *n1 - '0' : 0;
			flag1 = 1;
		}
		if (n2 > number2.begin())
			num2 = *n2-- - '0';
		else {
			num2 = !flag2 ? *n2 - '0' : 0;
			flag2 = 1;
		}

		numR = x ? -num1 + num2 : num1 - num2;

		if (flag)
			numR--;
		if (numR < 0) {
			numR = numR + 10 + '0';
			flag = 1;
		}
		else {
			numR += '0';
			flag = 0;
		}
		numberR = numR + numberR;


	}
	numberR.erase(0, numberR.find_first_not_of('0'));
	x = sign == '-' ? !x : x;
	return x ? '-' + numberR : numberR;
}

string VeLoInt::div(string number2, int y) const {

	VeLoInt tsum = *this;
	tsum.sign = '+';
	string tempNum = "";
	auto n = number.length() - number2.length();
	for (auto i = 0; i <= n; i++) {
		string temp = number2;
		temp.resize(number.length() - i, '0');
		int j = 0;
		while (tsum >= temp) {
			tsum = tsum - temp;
			j++;
		}

		tempNum += to_string(j);
	}
	tempNum.erase(0, tempNum.find_first_not_of('0'));
	return y ? tempNum : tsum.number;

}