#include "Circle.h"
#include <math.h>

void Circle::setRadius(double Rad)
{
	Radius = Rad;
	Ference = 2 * PI*Radius;
	Area = PI*Radius*Radius;
}
void Circle::setFerence(double Fer)
{
	Ference = Fer;
	Radius = Ference/ (2 * PI);
	Area = PI*Radius*Radius;
}
void Circle::setArea(double Ar)
{
	Area = Ar;
	Radius = sqrt(Area / PI);
	Ference = 2 * PI*Radius;
}

double Circle::getRadius() const
{
	return Radius;
}
double Circle::getFerence() const
{
	return Ference;
}
double Circle::getArea() const
{
	return Area;
}