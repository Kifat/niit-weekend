#define _CRT_SECURE_NO_WARNINGS
#include "DateTime.h"
#include <time.h>
#include <string>
#include <iostream>
#include <sstream>
using namespace std;

string DateTime::WeekDays[7] = { "�����������", "�������", "�����",
								"�������", "�������", "�������","�����������"};
string DateTime::WeekMonth[12] = { "������", "�������",
									"����", "������", "���", "����", 
									"����", "������", "��������",
									"�������", "������", "�������" };


string DateTime::getToday()const									//��� ������ �� ����� ������� ���� � ���� ������, � ��������� ��� ������ � �������� ������;
{
	ostringstream d;							
	ostringstream y;							
	d << Day;
	y << Year;
	string dstr = d.str();							// �������� ���� � ���� string
	string ystr = y.str();							// �������� ��� � ���� string
	return ("C������ " + WeekDays[WDay] +", " + dstr + " �����, "+ WeekMonth[Month] + " �����, " + ystr + " ���.");
}

string DateTime::getYesterday()const							//��� ������ �� ����� ���� ���������� ��� � ���� ������, � ��������� ��� ������ � �������� ������;
{
	ostringstream d;
	ostringstream y;
	d << (Day - 1);
	y << Year;
	string dstr = d.str();							// �������� ���� � ���� string
	string ystr = y.str();							// �������� ��� � ���� string
	return ("����� ��� ���� ������ - " + WeekDays[WDay-1] + ", " + dstr + " �����, " + WeekMonth[Month] + " �����, " + ystr + " ���.");
}			
string DateTime::getTomorrow()	const							//��� ������ �� ����� ���� ����������� ��� � ���� ������, � ��������� ��� ������ � �������� ������.
{
	ostringstream d;
	ostringstream y;
	d << (Day + 1);
	y << Year;
	string dstr = d.str();							// �������� ���� � ���� string
	string ystr = y.str();							// �������� ��� � ���� string
	return ("������ ����� ���� ������ - " + WeekDays[WDay + 1] + ", " + dstr + " �����, " + WeekMonth[Month] + " �����, " + ystr + " ���.");
}
string DateTime::getFuture(int y_day)	const					//��� ������ ���� ����� N ���� � �������;
{
	struct tm* tinfo;
	time_t FutSec = Seconds + y_day * (60 * 60 * 24);
	tinfo = localtime(&FutSec);
	
	ostringstream d;
	ostringstream y_d;
	ostringstream y;
	d << tinfo->tm_mday;
	y_d << y_day;
	y << Year;
	string dstr = d.str();							// �������� ���� � ���� string
	string y_dstr = y_d.str();						// �������� ��� ������ � ���� string	
	string ystr = y.str();							// �������� ��� � ���� string
	return ("����� " + y_dstr + " ����, ����� "+ dstr + " �����, " + WeekMonth[tinfo->tm_mon] + " �����, " + ystr + " ���.");
}
//int DateTime::getPast(int d_day)	const						//��� ������ ���� ����� N ���� � �������;
//{																// ���� �������� ������ �����, ���������� ���   	
//	struct tm* tinfo;											//int DateTime::getFuture(int y_day)	const
//	time_t PastSec = Seconds - d_day * (60*60*24);
//	tinfo = localtime(&PastSec);
//	int dday = tinfo->tm_mday;
//	return dday; 
//}
string DateTime::getPast(int d_day)	const					//��� ������ ���� ����� N ���� � �������;
{
	struct tm* tinfo;
	time_t PastSec = Seconds - d_day * (60 * 60 * 24);
	tinfo = localtime(&PastSec);

	ostringstream d;
	ostringstream d_d;
	ostringstream y;
	d << tinfo->tm_mday;
	d_d << d_day;
	y << Year;
	string dstr = d.str();							// �������� ���� � ���� string
	string d_dstr = d_d.str();						// �������� ��� ������ � ���� string	
	string ystr = y.str();							// �������� ��� � ���� string
	return ( d_dstr + " ���� ���� �����, ���� " + dstr + " �����, " + WeekMonth[tinfo->tm_mon] + " �����, " + ystr + " ���.");
}

string DateTime::getMonth()	const								//��� ������ �������� �������� ������;
{
	return ("�� ����� ����� - " + WeekMonth[Month]);
}
string DateTime::getWeekDay()	const							//��� ������ �������� �������� ��� ������;
{
	return ("������� ���� ������ - " + WeekDays[WDay]);
}

int DateTime::calcDifference(const DateTime& dt)							// ��� ������� �������(� ����) ����� ����� ������
{
	if (&dt == this)
		throw 8;

	int sec;
	sec = Seconds - dt.Seconds;
	int day = sec / (24*60*60);
	return day;
}

void DateTime::DTEnter(int year, int day, int month, int h, int m, int s)
{
	if (year < 1970)
		throw 6;

	if (day == 0)
		throw 7;

	if (month != 2 && month < 13 && month > 0)
	{
		if (month % 2)
		{
			if (day > 31)
				throw 1;
		}
		else
		{
			if (day > 30)
				throw 2;
		}
	}
	else if (month == 2)
	{
		if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
		{
			if (day > 29)
				throw 3;
		}
		else
		{
			if (day > 28)
				throw 4;
		}
	}
	else
	{
		throw 5;
	}


	Day = day;
	Month = month - 1;
	Year = year;

	time_t rawtime;
	struct tm* tinfo;
	time(&rawtime);
	tinfo = localtime(&rawtime);
	tinfo->tm_year = Year - nullYear;		// ������������ �������� ����
	tinfo->tm_mon = Month;                // ������������ �������� ������
	tinfo->tm_mday = Day;                   // ������������ �������� ���
	mktime(tinfo);							// ���������� ���� �����
	Seconds = mktime(tinfo);				// ���������� ���������� ������ (� �������� �� 1970 �)
	WDay = tinfo->tm_wday-1;					// ���� ������
}
void DateTime::DTLocal()
{
	Seconds = time(NULL);
	time_t rawtime;
	struct tm* tinfo;
	time(&rawtime);
	tinfo = localtime(&rawtime);
	Month = tinfo->tm_mon;                  // �����
	Day = tinfo->tm_mday;                   // ����
	WDay = tinfo->tm_wday-1;					// ���� ������	
	Year = nullYear + tinfo->tm_year;		// ���
}