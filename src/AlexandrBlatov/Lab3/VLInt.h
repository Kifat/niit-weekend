#include <string>
#include <iostream>
#ifndef _VLINT_H
#define _VLINT_H
using namespace std;

class VLInt
{
private:

	int* Number;
	int Count;
	bool Sign;  //false ����� ������ ����, true ����� ������ ����
	
public:
	VLInt(){ Number = 0; Count = 0; }
	VLInt(int num) { Number = new int[Count = num]; Sign = false; }		//�����������
//	VLInt(const VLInt& two)
//	{
//		setNumber(two.Number);
//		Number = new int[two.Count];
//		for (int i = 0; i<Count; i++)
//			Number[i] = two.Number[i];
//		Sign = two.Sign;
//	}
	~VLInt()															//����������
	{
		delete [] Number; 
	}						
	void setNumber(const char * num);							//���� �����
	void setNumber(int * num, int c);
	void setNumber(string str);
	string getNumber() const;							//����� �����	

	VLInt& VLInt::operator = (const VLInt &two);		//���������� �������� =
	VLInt& VLInt::operator + (const VLInt &two);		//���������� �������� +
	VLInt& VLInt::operator - (const VLInt &two);		//���������� �������� -
	VLInt& VLInt::operator += (const VLInt &two);		//���������� �������� += 
	VLInt& VLInt::operator -= (const VLInt &two);		//���������� �������� -= 
	VLInt& VLInt::operator * (const VLInt &two);		//���������� �������� *
	VLInt& VLInt::operator / (const VLInt &two);		//���������� �������� / 
	VLInt& VLInt::operator % (const VLInt &two);		//���������� �������� %
	VLInt& VLInt::operator *= (const VLInt &two);		//���������� �������� *= 
	VLInt& VLInt::operator /= (const VLInt &two);		//���������� �������� /= 
	bool VLInt::operator < (const VLInt &two);		//���������� �������� <
	bool VLInt::operator > (const VLInt &two);		    //���������� �������� >
	bool VLInt::operator <= (const VLInt &two);		//���������� �������� <= 
	bool VLInt::operator >= (const VLInt &two);		//���������� �������� >= 
	bool VLInt::operator == (const VLInt &two);		//���������� �������� ==
	bool VLInt::operator != (const VLInt &two);		//���������� �������� !=
	VLInt& VLInt::operator ^ (const int);		//���������� �������� ^
	private:
		int arrTOnum(int* arr,int count, int i);		//������� �� ������� ����� � �������� �������
	//	void VLInt::intTONum(int z);					//����������� ����� � ������ Number
		void copy_arr(int* arr, int count);				//�������� ������ arr � ������ Number
	
};
#endif