#include "Automat.h"
#include <string>
#include <iostream>
#include <sstream>
#include <Windows.h>

using namespace std;

string Automat::Menu[7] = {			"��� (�������) ", "��� (������) ",
									"���� - �������� ", "���� - ����� ",
									"���� - �� �������� ", "���� - � ����� ", "���� - ��� ���� " };
string Automat::Price_str[7] = {	"- 25", "- 23",
									"- 35", "- 40",
									"- 30", "- 15", "- 10" };
int Automat::Price[7] =		{		25, 23,
									35, 40,
									30, 15, 10};

void Automat::coin(int coin)			//��������� ����� �� ���� �������������;
{
	if (coin < 0)
		throw 3;

	if (Stat == WAIT || Stat == ACCEPT || Stat == CHECK)
	{
		Stat = ACCEPT;
		Change = 0;
		Account = Account + coin;
		str_Account = intTOstr(Account);
	}
	else if (Stat == COOK)
		throw 1;
	else if (Stat == OFF)
		throw 2;
	
}

string Automat::getMenu() const	//����������� ���� � ��������� � ������ ��� ������������;
{	
	string FullMenu;
	
	for (int i = 0; i < 6; ++i)
		
		FullMenu = FullMenu + Menu[i] + Price_str[i] + '\n';
	
	return FullMenu;
}
string Automat::getState() const			//����������� �������� ��������� ��� ������������;
{
	switch (Stat)
	{
	case WAIT:
		if (Change == 0)
			return "������� ����� � ������";
		else
		{
			return "���� ����� - " + str_Change + ". ������� ����� � ������.";
		}
	break;
	case ACCEPT:
		if (FlagChoice == false)
		{
			return "������� ��������� � ������ ������ �����, �� ����� - " + str_Account + " ���.";
		}
		else
		{
			string str_NoPr = intTOstr((Price[Choice] - Account));   // �������� "���������" � ���� string
			return "��� ������������ " + Menu[Choice] + " ���������� ������ �� ���� - " + str_NoPr + " ���.";
		}
	break;

	case CHECK:
			return "������� ��������� � ������ �������� ����������, �� ����� - " + str_Account + " ���." + '\n' + "��������� ���������� ������� " + Price_str[Choice];
	break;

	case COOK:
		if (FlagChoice == false)
		{
			if (Change == 0)
				return "������� ������� " + Menu[Choice] + ". ��������� �������, ����������!";
			else
			{
				return "������� ������� " + Menu[Choice] + ".��������� �������, ����������!"+ '\n' +"���� ����� - " + str_Change + " ���.";
			}
		}
		else
		{
			return "�� �������: " + Menu[Choice] + ", ��������� ���������� ������� " + Price_str[Choice] + ".";
		}
	break;

	case OFF:
		return "������� ��������";
	break;
	}
	return 0;
}

void Automat::choice(int n = 1) //����� ������� �������������;
{	
	if (n <= 0 || n > 6)
		throw 4;

	Stat = COOK;
	Choice = n-1;
	FlagChoice = true;
}
void Automat::check()//�������� ������� ����������� �����;
{
	if (Price[Choice] > Account)
	{
		Stat = ACCEPT;
		
	}
	else if (Price[Choice] < Account)
	{
		Stat = CHECK;
		Change = Account - Price[Choice];
		str_Change = intTOstr(Change);
	}
	else if (Price[Choice] == Account)
		Stat = CHECK;
}
void Automat::cancel()				//������ ������ ������������ �������������;
{
	Stat = WAIT;
		
	if (Price[Choice] > Account)
	{
		Change = Account;	//����� ���������� getState()
		str_Change = intTOstr(Change);
		Account = 0;
		str_Account = "0";
		Choice = 0;
		FlagChoice = false;
	}
	else
	{
		Change = 0;				
		str_Change = "0";
		Account = 0;
		str_Account = "0";
		Choice = 0;
		FlagChoice = false;
	}
}
void Automat::cook()               //�������� �������� ������������� �������;
{
	Stat = COOK;
	FlagChoice = false;
	Sleep(5000);
}
void Automat::finish()				       //���������� ������������ ������������.
{
	Stat = WAIT;
	Change = 0;
	str_Change = "0";
	Account = 0;
	str_Account = "0";
	Choice = 0;
	FlagChoice = false;
}