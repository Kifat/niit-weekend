#include "Circle.h"
#include <math.h>


using namespace std;

void Circle::setRadius(double Rad)
{
	Radius = Rad;
	Area = 3.14*Radius*Radius;
	Ference = 2 * 3.14*Radius;
}

void Circle::setArea(double Ar)
{
	Area = Ar;
	Radius = sqrt(Area / 3.14);
	Ference = 2 * 3.14*Radius;
}

void Circle::setFerence(double Fer)
{
	Ference = Fer;
	Radius = Ference / (2 * 3.14);
	Area = 3.14*Radius*Radius;
}

double Circle::getRadius() const
{
	return Radius;
}

double Circle::getArea() const
{
	return Area;
}

double Circle::getFerence() const
{
	return Ference;
}