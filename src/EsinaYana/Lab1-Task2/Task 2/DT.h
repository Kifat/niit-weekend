﻿#ifndef _DT_H_
#define _DT_H_
#pragma warning(disable : 4996)
#include <ctime>

class DateTime
{
private:
	time_t sec;
	int Day;
	int Month;
	int Year;
	time_t setDT();
	time_t setDTNow();
public:
	DateTime(int Day, int Month, int Year)
	{
		this->Day = Day;
		this->Month = Month-1;
		this->Year = Year;
		this->sec=setDT();
	}
	DateTime()
	{
		this->sec=setDTNow();
	}
	DateTime& operator=(const DateTime& rs) {}	
	int getToday() const;
	int getYesterday() const;
	int getTomorrow() const;
	int getFuture(int nDay) const;
	int getFuture(int nDay, int Day, int Month, int Year) const;
	int getPast(int nDay) const;
	int getPast(int nDay, int Day, int Month, int Year) const;
	int getMonth() const;
	int getWeekDay() const;
	static int calcDifference(int Day1, int Month1, int Year1, int Day2, int Month2, int Year2);
	int calcDifference(int Day1, int Month1, int Year1);
	~DateTime() {}
};


#endif