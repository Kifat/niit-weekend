﻿#ifndef _AUTOMAT_H_
#define _AUTOMAT_H_

#include <string>
using namespace std;

enum STATES { OFF, WAIT, ACCEPT, CHECK, COOK };

class Automata
{
private:	
	static STATES state;//текущее состояние
	static int prices[5];//массив цен напитков	
	static string message;
public:
	static int cash;//текущая сумма
	static int position;//выбранная позиция
	static int surrender;//сдача

	static bool check(int pos);//проверка наличия необходимой суммы

public:
	Automata()//конструктор
	{
		state = OFF;
		cash = 0;
		surrender = 0;
		message = "";
	};
	string on() //включение автомата
	{ 
		state = WAIT;
		message = "Автомат включен.\n";
		return message;
	};
	void off() { state = OFF; cash = 0; surrender = 0; };//выключение автомата
	string coin(int sum);//занесение денег на счёт пользователем
	string getMenu() const;//отображение меню с напитками и ценами для пользователя
	string getState() const;//отображение текущего состояния для пользователя
	string choise(int position);//выбор напитка пользователем
	string cancel();//отмена сеанса обслуживания пользователем
	string cook();//имитация процесса приготовления напитка
	void finish();//завершение обслуживания пользователя
	~Automata() {};//деструктор
};
#endif