#ifndef _HEADER_H_
#define _HEADER_H_

class Circle
{
private:
	double Radius;
	double Ference;
	double Area;
	void calculation_ference();
	void calculation_area();
	void calculation_radius();
public:

	double get_Radius()const;
	void set_Radius(double);

	double get_Ference()const;
	void set_Ference(double);

	double get_Area()const;
	void set_Area(double);

	void message_menu();
	void reset();
};


#endif
